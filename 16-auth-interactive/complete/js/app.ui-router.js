'use strict';

function authInterceptor($injector, $q) {

  return {
    responseError: function(response) {
      return $injector.invoke(function(authModal, authService, $http) {

        function isRecoverable(response) {
          return response.status === 401 && !authService.isLoginUrl(response.config.url);
        }

        if (isRecoverable(response)) {
          return authModal.showLoginModal()
            .then(function() {
              return $http(response.config);
            });
        }

        return $q.reject(response);
      });
    }
  };
}

function tokenInterceptor($injector) {
  var AUTH_HEADER = 'X-Auth-Token';

  return {
    request: function(config) {

      var authService = $injector.get('authService');
      if (authService.isAuthenticated()) {
        config.headers = config.headers || {};
        config.headers[AUTH_HEADER] = authService.getToken();
      }

      return config;
    }
  };
}

function configInterceptors($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('tokenInterceptor');
}

function configRouter($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('list', {
      url: '/',
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list',
      resolve: {
        ordersData: function(Order) {
          return Order.query().$promise;
        }
      }
    })

    .state('detail', {
      url: '/detail/:id',
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Order, $stateParams) {
          var id = $stateParams.id;
          return Order.get({'id': id}).$promise;
        }
      }
    })

    .state('create', {
      url: '/create',
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create',
      data: {
        requiresAuthentication: true
      }
    });
}

angular.module('authApp', ['ui.router', 'ngMessages', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', '//angular-cz-security-api.herokuapp.com')

  .factory("authInterceptor", authInterceptor)
  .factory("tokenInterceptor", tokenInterceptor)

  .run(function($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function() {
      $location.path("/")
    });

  })
  .run(function($rootScope, $location, $state, authService, authModal) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

      var requiresAuthentication = Boolean(toState.data && toState.data.requiresAuthentication);

      if (requiresAuthentication && !authService.isAuthenticated()) {
        event.preventDefault();

        authModal.showLoginModal()
          .then(continueRouting)
          .catch(goToPreviousRoute);
      }

      function continueRouting() {
        $state.go(toState, toParams);
      }

      function goToPreviousRoute() {
        if (fromState.name) {
          $state.go(fromState, fromParams);
        } else {
          $location.path('/');
        }
      }
    });
  })

  .config(configInterceptors)
  .config(configRouter);
