describe('reverseFilter', function() {

  beforeEach(module('filterApp'));

  it('should be function with two arguments (TODO 2.1)', inject(function(reverseFilter) {
    expect(reverseFilter).toEqual(jasmine.any(Function));
    expect(reverseFilter.length).toBe(2);
  }));

  it('should return reverted string (TODO 2.2)', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('123', '')).toEqual('321');
      expect(reverseFilter('dlouhý text s mezerami', '')).toEqual('imarezem s txet ýhuold');
    });
  });

  it('should return empty string, if input is undefined (TODO 2.2)', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter(undefined, '')).toEqual('');
    });
  });

  it('should return reverted string with prefix (TODO 2.3)', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('', 'prefix')).toEqual('prefix');
      expect(reverseFilter('123', 'prefix')).toEqual('prefix321');
    });
  });

  it('should return empty string when input is empty and there is no prefix (TODO 2.3)', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('')).toEqual('');
    });
  });

});
