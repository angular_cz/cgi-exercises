'use strict';

function configRouter($routeProvider) {

  $routeProvider
    .when('/orders', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list'
    })
    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Order, $route) {
          var id = $route.current.params.id;

          return Order.get({'id': id}).$promise;
        }
      }
    })
    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create'
    })
    .otherwise('/orders');
}

function Order(REST_URI, $resource) {
  return $resource(REST_URI + '/orders/:id', {"id": "@id"});
}

function OrderListController(Order) {
  var orderCtrl = this;
  this.orders = Order.query();

  this.statuses = {
    NEW: 'Nová',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.removeOrder = function(order) {
    order.$remove().then(function() {
      var index = orderCtrl.orders.indexOf(order);
      orderCtrl.orders.splice(index, 1);
    });
  };

  this.updateOrder = function(order) {
    order.$save();
  };

}

function OrderDetailController(orderData) {
  this.order = orderData;
}

function OrderCreateController($location, Order) {
  this.order = new Order();

  this.save = function(order) {
    order.$save().then(function() {
      $location.path("/detail/" + order.id);
    });
  }
}

angular.module('orderAdministration', ['ngRoute', 'ngMessages', 'ngResource'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .config(configRouter)
  .factory('Order', Order)
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController)
  .controller('OrderCreateController', OrderCreateController);
