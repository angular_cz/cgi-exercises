var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.plugins.push('karma-ng-html2js-preprocessor');

  config.files.push('10-component/complete/app.js');
  config.files.push('10-component/complete/*.spec.js');
  config.files.push('10-component/complete/*.html');

  if (!config.preprocessors['**/*.html']) {
    config.preprocessors['**/*.html'] = [];
  }

  config.preprocessors['**/*.html'].push('ng-html2js');

  config.ngHtml2JsPreprocessor = {
    stripPrefix: '10-component/complete/',
    moduleName: 'componentApp.templates'
  }
};
