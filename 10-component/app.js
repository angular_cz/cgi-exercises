'use strict';

// TODO 1.2 - změňte definiční objekt na komponentu
// TODO 3.1 - přidejte závislost na komponentě tabset
var tab = {
  templateUrl: 'tab.html',
  transclude: true,

  scope: {},
  bindToController: {
    header: '@'
  },

  controllerAs: 'tab',
  controller: function() {
  }
};

// TODO 2.3 - přidejte komponentě controller
var tabset = {
  templateUrl: 'tabset.html',
  transclude: true
};


function TabsetController($log) {
  this.tabs = [];

  var hide = function(tab) {
    tab.active = false;
  };

  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.forEach(hide);

    tab.active = true;
  };
}

function InfoController($log) {
  this.reportChange = function(tabHeader) {
    this.currentTabHeader = tabHeader;

    $log.info('Tab changed:', tabHeader);
  }
}

// TODO 1.1 - změňte directivu tab na komponentu
// TODO 2.1 - přidejte komponentu tabset

angular.module('directiveApp', [])

  .directive('tab', function() {return tab})
  .controller('InfoController', InfoController);
