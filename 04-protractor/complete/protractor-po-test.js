'use strict';

var UserNamePO = function() {
  this.name = element(by.binding('vm.user.name'));
  this.nameInput= element(by.model('vm.user.name'));
  this.surname = element(by.binding('vm.user.surname'));
  this.surnameInput= element(by.model('vm.user.surname'));

  this.setNameInput = function(text) {
    this.nameInput.clear().sendKeys(text);
  };

  this.getNameInput = function() {
    return this.nameInput.getAttribute('value');
  };

  this.getName = function() {
    return this.name.getText();
  };

  this.getSurnameInput = function() {
    return this.surnameInput.getAttribute('value');
  };

  this.getSurname = function() {
    return this.surname.getText();
  };

  this.goTo = function() {
    browser.get('index.html');
  };

  this.yearOfBirthInput = element(by.model('vm.user.yearOfBirth'));
  this.age =  element(by.binding('vm.getAge()'));

  this.setYearOfBirth = function(year) {
    this.yearOfBirthInput.clear().sendKeys(year);
  };

  this.getAge = function() {
    return this.age.getText();
  };
};

describe('04-protractor example tested with page object', function() {

  var pageObject;
  beforeEach(function() {
    pageObject = new UserNamePO();
    pageObject.goTo();
  });

  it('should have initial settings', function() {
    expect(pageObject.getNameInput()).toBe('Petr');
    expect(pageObject.getName()).toMatch('Petr');

    expect(pageObject.getSurnameInput()).toBe('Novák');
    expect(pageObject.getSurname()).toBe('Příjmení: Novák');
  });

  it('should change age if model is changed', function() {
    var now = new Date();
    var expectedYear = now.getFullYear() - 1970;

    pageObject.setYearOfBirth('1970');
    expect(pageObject.getAge()).toMatch(expectedYear.toString());
  });

  it('should change name if model is changed', function() {
    pageObject.setNameInput('Josef');
    expect(pageObject.getName()).toMatch('Josef');
  });

});
