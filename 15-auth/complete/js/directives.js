'use strict';

function authInfoDirective(authService) {
  return {
    scope: true,
    link: function(scope) {
      scope.auth = authService
    },
    template: '{{auth.getUserName()}} <a class="btn-logout" ng-click="auth.logout()">Logout</a>'
  };
}

function authIsAuthenticatedDirective(authService) {
  return {
    scope: true,
    link: function(scope, element, attrs) {
      var authenticatedState = attrs.authIsAuthenticated === 'false';

      function changeVisibility() {
        var toggleHide = authService.isAuthenticated() === authenticatedState;

        element.toggleClass("auth-hide", toggleHide);
      }

      changeVisibility();

      scope.$on("login:loginSuccess", changeVisibility);
      scope.$on("login:loggedOut", changeVisibility);
    }
  };
}

function authIsAuthenticatedLikeIfDirective() {
  return {
    transclude: true,
    controller: function($scope, $transclude,
                         $attrs, $element, authService) {

      var childScope;
      var authenticatedState = $attrs.authIsAuthenticated === 'false';

      function changeVisibility() {
        var toggleHide = authService.isAuthenticated() === authenticatedState;

        if (toggleHide) {
          $element.empty();
          if (childScope) {
            childScope.$destroy();
            childScope = null;
          }
        } else {
          $transclude(function(clone, newScope) {
            childScope = newScope;
            $element.append(clone);
          });
        }
      }

      changeVisibility();

      $scope.$on("login:loginSuccess", changeVisibility);
      $scope.$on("login:loggedOut", changeVisibility);

    }
  };
}

function authHasRoleDirective(authService) {
  return {
    scope: true,
    link: function(scope, element, attrs) {

      function changeVisibility() {
        var hasRole = authService.hasRole(attrs.authHasRole);
        element.toggleClass("auth-hide", !hasRole);
      }

      changeVisibility();

      scope.$on("login:loginSuccess", changeVisibility);
      scope.$on("login:loggedOut", changeVisibility);
    }
  };
}

angular.module('authApp')
  .directive("authInfo", authInfoDirective)
  .directive("authIsAuthenticated", authIsAuthenticatedDirective)
  .directive("authIsAuthenticatedLikeIf", authIsAuthenticatedLikeIfDirective)
  .directive("authHasRole", authHasRoleDirective);
