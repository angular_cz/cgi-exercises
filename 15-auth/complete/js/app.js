'use strict';

function configRouter($routeProvider) {

  $routeProvider
    .when('/orders', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list'
    })

    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Order, $route) {
          var id = $route.current.params.id;

          return Order.get({'id': id}).$promise;
        }
      }
    })

    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create'
    })

    .when('/', {
      templateUrl: 'home.html',
      controller: 'HomeCtrl',
      controllerAs: 'home'
    })

    .otherwise('/');
}

function tokenInterceptor($injector) {
  var AUTH_HEADER = 'X-Auth-Token';

  return {
    request: function(config) {
      var authService = $injector.get('authService');

      if (authService.isAuthenticated()) {
        config.headers = config.headers || {};
        config.headers[AUTH_HEADER] = authService.getToken();
      }

      return config;
    }
  };
}

function configInterceptors($httpProvider) {
  $httpProvider.interceptors.push(tokenInterceptor);
}

angular.module('authApp', ['ngRoute', 'ngMessages', 'ngResource', 'ngStorage', 'ui.bootstrap'])
  .constant('REST_URI', '//angular-cz-security-api.herokuapp.com')
  
  .config(configRouter)
  .config(configInterceptors)

  .run(function($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function() {
      $location.path("/");
    });
  });
